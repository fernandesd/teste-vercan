<?php

return [
    'ibge' => [
        'baseUrl' => 'http://servicodados.ibge.gov.br/api/v1',
    ],
    'viaCep' => [
        'baseUrl' => 'https://viacep.com.br/ws',
    ],
    'receitaAws' => [
        'baseUrl' => 'https://receitaws.com.br/v1/cnpj',
    ],
];
