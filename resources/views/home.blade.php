@extends('layouts.app')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard - <small>Painel de Controle</small> </h1>
@stop

@section('content')
    <section class="p-4 bg-white">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header"> Olá Candidato, bem-vindo ao Desafio Vercan - <small class="float-end">Hoje é
                        {{now()->format('d/m/Y')}}</small> </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address> <strong>Suporte</strong><br> Celular: (19) 9 9820-4361<br> Celular: (19) 9 9612-4430<br> E-mail:
                    suporte@vercan.com.br </address>
            </div>
            <div class="col-sm-4 invoice-col">
                <address> <strong>Endereço</strong><br> Rua Dr. Benigno Ribeiro, 176 <br> Bairro: São Bernardo<br> Campinas
                    - São Paulo </address>
            </div>
            <div class="col-sm-4 invoice-col"> <strong>Mais Informações</strong><br> Telefone: (19) 3291-0004<br> E-mail:
                contato@vercan.com.br </div>
        </div> Vercan Tecnologia.
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
