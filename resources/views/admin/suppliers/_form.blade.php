<x-adminlte-card title="Dados do Fornecedor" theme="light" collapsible>
    <div class="row">
        <div class="col-12">
            <div class="form-check form-check-inline">
                {{ Form::radio('type', '0', true, ['class' => 'form-check-input radio', $edit ? 'disabled' : '']) }}
                {{ Form::label('Pessoa Jurídica', null, ['class' => 'form-check-label radio']) }}

                {{ Form::radio('type', '1', false, ['class' => 'form-check-input ml-4', $edit ? 'disabled' : '']) }}
                {{ Form::label('Pessoa Física', null, ['class' => 'form-check-label']) }}
            </div>
        </div>
    </div>


    <div id="cnpj">
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('cnpj', 'CNPJ', ['class' => 'control-label']) }}
                    {{ Form::text('cnpj', null, ['class' => 'form-control cnpj']) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('cnpj_legal_name', 'Razão Social', null, ['class' => 'control-label']) }}
                    {{ Form::text('cnpj_legal_name', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('cnpj_trade_name', 'Nome Fantasia', null, ['class' => 'control-label']) }}
                    {{ Form::text('cnpj_trade_name', null, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Indicador de Inscrição Estadual', null, ['class' => 'control-label']) }}
                    {{ Form::select('cnpj_ie_indicator', \App\Models\Supplier::IE_INDICATOR, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Inscrição Estadual', null, ['class' => 'control-label']) }}
                    {{ Form::text('cnpj_ie', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Inscrição Municipal', null, ['class' => 'control-label']) }}
                    {{ Form::text('cnpj_im', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Situação CNPJ', null, ['class' => 'control-label']) }}
                    {{ Form::text('cnpj_status', null, ['class' => 'form-control', 'readonly']) }}
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Recolhimento', null, ['class' => 'control-label']) }}
                    {{ Form::select('cnpj_tax_collection_type', \App\Models\Supplier::TAX_COLLECTON_TYPE, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Ativo', null, ['class' => 'control-label']) }}
                    {{ Form::select('is_active', \App\Models\Supplier::IS_ACTIVE, 1, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>
        </div>
    </div>
    <div id="cpf">
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('CPF', null, ['class' => 'control-label']) }}
                    {{ Form::text('cpf', null, ['class' => 'form-control cpf']) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('Nome', null, ['class' => 'control-label']) }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Apelido', null, ['class' => 'control-label']) }}
                    {{ Form::text('nickname', null, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('RG', null, ['class' => 'control-label']) }}
                    {{ Form::text('rg', null, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Ativo', null, ['class' => 'control-label']) }}
                    {{ Form::select('is_active', \App\Models\Supplier::IS_ACTIVE, 1, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>
        </div>
    </div>
</x-adminlte-card>

<x-adminlte-card title="Contato Principal" theme="light" collapsible>
    <div class="row">
        <div class="col-md-6">
            <button type="button" class="contactPhoneList1_add btn btn-sm btn-outline-info">Adicionar</button>
            <div id="contactPhoneList1">
                @forelse ($mainContacts ?? [] as $contact)
                <div class="row contactPhoneList1_var">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('Telefone', null, ['class' => 'control-label']) }}
                            {{ Form::text('contact[main][$loop][phone]', $contact->phone, ['class' => 'form-control tel', 'data-name-format' => 'contact[main][%d][phone]']) }}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                            {{ Form::select('contact[main][$loop][phone_type]', \App\Models\Contact::PHONE_TYPES, $contact->phone_type, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[main][%d][phone_type]']) }}
                        </div>
                    </div>
                    <button type="button" class="contactPhoneList1_del btn btn-sm btn-outline-danger">Remover</button>
                </div>
                @empty
                    <div class="row contactPhoneList1_var">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Telefone', null, ['class' => 'control-label']) }}
                                {{ Form::text('contact[main][0][phone]', null, ['class' => 'form-control tel', 'data-name-format' => 'contact[main][%d][phone]']) }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                                {{ Form::select('contact[main][0][phone_type]', \App\Models\Contact::PHONE_TYPES, null, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[main][%d][phone_type]']) }}
                            </div>
                        </div>
                        <button type="button" class="contactPhoneList1_del btn btn-sm btn-outline-danger">Remover</button>
                    </div>
                @endforelse
            </div>
        </div>
        <div class="col-md-6">
            <button type="button" class="contactEmailList1_add btn btn-sm btn-outline-info">Adicionar</button>

            <div id="contactEmailList1">
                @forelse ($mainContacts ?? [] as $contact)
                <div class="row contactEmailList1_var">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('Email', null, ['class' => 'control-label']) }}
                            {{ Form::email('contact[main][$loop][email]', $contact->email, ['class' => 'form-control', 'data-name-format' => 'contact[main][%d][email]']) }}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                            {{ Form::select('contact[main][$loop][email_type]', \App\Models\Contact::EMAIL_TYPES, $contact->email_type, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[main][%d][email_type]']) }}
                        </div>
                    </div>
                    <button type="button" class="contactEmailList1_del btn btn-sm btn-outline-danger">Remover</button>
                </div>
                @empty
                    <div class="row contactEmailList1_var">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Email', null, ['class' => 'control-label']) }}
                                {{ Form::email('contact[main][0][email]', null, ['class' => 'form-control', 'data-name-format' => 'contact[main][%d][email]']) }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                                {{ Form::select('contact[main][0][email_type]', \App\Models\Contact::EMAIL_TYPES, null, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[main][%d][email_type]']) }}
                            </div>
                        </div>
                        <button type="button" class="contactEmailList1_del btn btn-sm btn-outline-danger">Remover</button>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</x-adminlte-card>

<x-adminlte-card title="Contatos Adicionais" theme="light" collapsible>
    <div class="">
        <div class="row mt-3">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('Nome', null, ['class' => 'control-label']) }}
                    {{ Form::text('contact[additional][name]', $additionalContact->name ?? null, ['class' => 'form-control', 'data-name-format' => 'contact[additional][%d][name]']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Empresa', null, ['class' => 'control-label']) }}
                    {{ Form::text('contact[additional][company]', $additionalContact->company ?? null, ['class' => 'form-control', 'data-name-format' => 'contact[additional][%d][company]']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Cargo', null, ['class' => 'control-label']) }}
                    {{ Form::text('contact[additional][role]', $additionalContact->role ?? null, ['class' => 'form-control', 'data-name-format' => 'contact[additional][%d][role]']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <button type="button" class="contactPhoneList2_add btn btn-sm btn-outline-info">Adicionar</button>
                <div id="contactPhoneList2">
                    @forelse ($additionalContacts ?? [] as $contact)
                    <div class="row contactPhoneList2_var">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Telefone', null, ['class' => 'control-label']) }}
                                {{ Form::text('contact[additional][informations][$loop][phone]', $contact->phone, ['class' => 'form-control tel', 'data-name-format' => 'contact[additional][informations][%d][phone]']) }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                                {{ Form::select('contact[additional][informations][$loop][phone_type]', \App\Models\Contact::PHONE_TYPES, $contact->phone_type, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[additional][informations][%d][phone_type]']) }}
                            </div>
                        </div>
                        <button type="button"class="contactPhoneList2_del btn btn-sm btn-outline-danger">Remover</button>
                    </div>
                    @empty
                    <div class="row contactPhoneList2_var">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Telefone', null, ['class' => 'control-label']) }}
                                {{ Form::text('contact[additional][informations][0][phone]', null, ['class' => 'form-control tel', 'data-name-format' => 'contact[additional][informations][%d][phone]']) }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                                {{ Form::select('contact[additional][informations][0][phone_type]', \App\Models\Contact::PHONE_TYPES, null, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[additional][informations][%d][phone_type]']) }}
                            </div>
                        </div>
                        <button type="button"class="contactPhoneList2_del btn btn-sm btn-outline-danger">Remover</button>
                    </div>
                    @endforelse
                </div>
            </div>
            <div class="col-md-6">
                <button type="button" class="contactEmailList2_add btn btn-sm btn-outline-info">Adicionar</button>
                <div id="contactEmailList2">
                    @forelse ($additionalContacts ?? [] as $contact)

                    <div class="row contactEmailList2_var">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('Email', null, ['class' => 'control-label']) }}
                                {{ Form::email('contact[additional][informations][$loop][email]', $contact->email, ['class' => 'form-control', 'data-name-format' => 'contact[additional][informations][%d][email]']) }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                                {{ Form::select('contact[additional][informations][$loop][email_type]', \App\Models\Contact::EMAIL_TYPES, $contact->email_type, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[additional][informations][%d][email_type]']) }}
                            </div>
                        </div>
                        <button type="button"
                            class="contactEmailList2_del btn btn-sm btn-outline-danger">Remover</button>
                    </div>

                    @empty
                        <div class="row contactEmailList2_var">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('Email', null, ['class' => 'control-label']) }}
                                    {{ Form::email('contact[additional][informations][0][email]', null, ['class' => 'form-control', 'data-name-format' => 'contact[additional][informations][%d][email]']) }}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    {{ Form::label('Tipo', null, ['class' => 'control-label']) }}
                                    {{ Form::select('contact[additional][informations][0][email_type]', \App\Models\Contact::EMAIL_TYPES, null, ['class' => 'form-control', 'placeholder' => 'Selecione', 'data-name-format' => 'contact[additional][informations][%d][email_type]']) }}
                                </div>
                            </div>
                            <button type="button"
                                class="contactEmailList2_del btn btn-sm btn-outline-danger">Remover</button>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</x-adminlte-card>
<x-adminlte-card title="Dados de Endereço" theme="light" collapsible>
    <div id="cpf">
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('CEP', null, ['class' => 'control-label']) }}
                    {{ Form::text('zipcode', $address->zipcode ?? null, ['class' => 'form-control cep']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Logradouro', null, ['class' => 'control-label']) }}
                    {{ Form::text('street', $address->street ?? null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Número', null, ['class' => 'control-label']) }}
                    {{ Form::text('number', $address->number ?? null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Complemento', null, ['class' => 'control-label']) }}
                    {{ Form::text('complement', $address->complement ?? null, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Bairro', null, ['class' => 'control-label']) }}
                    {{ Form::text('district', $address->district ?? null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Ponto de Referência', null, ['class' => 'control-label']) }}
                    {{ Form::text('reference', $address->reference ?? null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('UF', null, ['class' => 'control-label']) }}
                    {{ Form::select('state_id', \App\Models\State::pluck('key', 'id'), $address->state_id ?? null, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Cidade', null, ['class' => 'control-label']) }}
                    {{ Form::select('city_id', $cities ?? [], $address->city_id ?? null, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('Condomínio?', null, ['class' => 'control-label']) }}
                    {{ Form::select('is_condominium', \App\Models\Address::IS_CONDOMINIUM, $address->is_condominium ?? null, ['class' => 'form-control', 'placeholder' => 'Selecione']) }}
                </div>
            </div>
            <div class="col-md-9 row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('Endereço', null, ['class' => 'control-label']) }}
                        {{ Form::text('end_cond', $address->end_cond ?? null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('Número', null, ['class' => 'control-label']) }}
                        {{ Form::text('number_cond', $address->number_cond ?? null, ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-card>
<x-adminlte-card title="Observação" theme="light" collapsible>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::textarea('description', null, ['class' => 'form-control summernote']) }}
            </div>
        </div>
    </div>
</x-adminlte-card>
@push('js')
    <script>
        $(document).ready(function() {
            $("input[name='type']:checked").val() == 0 ? $('#cpf').hide() : $('#cnpj').hide();

            $("input[name='type']:radio").change(function() {
                if ($(this).val() == '0') {
                    $('#cpf').hide();
                    $('#cpf input').each(function() {
                        $(this).val('')
                    });
                    $('#cnpj').show();
                } else if ($(this).val() == '1') {
                    $('#cnpj').hide();
                    $('#cnpj input').each(function() {
                        $(this).val('')
                    });
                    $('#cnpj select[name=cnpj_ie_indicator], #cnpj select[name=cnpj_tax_collection_type]')
                        .val('');
                    $('#cpf').show();
                }
            });
        });
    </script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Digite uma observação...',
            tabsize: 2,
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });
    </script>
    <script>
        $('#contactPhoneList1').addInputArea({
            area_del: '.del-area',
            after_add: () => $('.tel').mask('(99) 9 9999-9999'),
        });
        $('#contactEmailList1').addInputArea({
            area_del: '.del-area',
        });
        $('#contactPhoneList2').addInputArea({
            after_add: () => $('.tel').mask('(99) 9 9999-9999'),
        });
        $('#contactEmailList2').addInputArea({
            area_del: '.del-area',
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.cpf').mask('000.000.000-00');
            $('.cnpj').mask('00.000.000/0000-00');
            $('.tel').mask('(99) 9 9999-9999');
            $('.cep').mask('00000-000');
        });
    </script>
    <script>
        $(document).ready(function() {
            $('input[name=cnpj]').on('change', function(e) {
                if ($(this).val().length >= 11) {
                    const cnpj = $(this).val().replace(/[^a-zA-Z0-9]/g, "");
                    const receitaUrl = 'http://localhost/api/dados-cnpj';
                    const myRequest = new Request(`${receitaUrl}/${cnpj}`);

                    fetch(myRequest)
                        .then((response) => response.json())
                        .then((data) => {
                            $('input[name=cnpj_legal_name]').val(data.nome);
                            $('input[name=cnpj_trade_name]').val(data.fantasia);
                            $('input[name=cnpj_status]').val(data.situacao);
                            $('input[name=zipcode]').val(data.cep.replace(/[^a-zA-Z0-9]/g, "")  );
                            $('input[name=zipcode]').trigger('change');
                            console.log(data)
                        })
                        .catch(error => {
                            console.log(error)
                        })
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('input[name=zipcode]').on('change', function(e) {
                if ($(this).val().length >= 8) {
                    const cep = $(this).val().replace(/[^a-zA-Z0-9]/g, "");
                    const cepUrl = 'http://localhost/api/dados-cep';
                    const myRequest = new Request(`${cepUrl}/${cep}`);

                    fetch(myRequest)
                        .then((response) => response.json())
                        .then((data) => {
                            $('input[name=street]').val(data.logradouro);
                            $('input[name=complement]').val(data.complemento);
                            $('input[name=district]').val(data.bairro);
                            $('select[name=state_id]').val(data.stateDbId).change();
                            $('select[name=city_id]').val(data.cityDbId).change();
                            console.log(data)
                        })
                        .catch(error => {
                            console.log(error)
                        })
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('select[name=state_id]').on('change', function(e) {
                    const estado = $(this).val();
                    const estadoUrl = 'http://localhost/api/estado';
                    const myRequest = new Request(`${estadoUrl}/${estado}/cidades`);

                    fetch(myRequest)
                        .then((response) => response.json())
                        .then((data) => {
                            $('select[name=city_id]').empty()
                            .append('<option selected="selected" value="">Selecione</option>');
                            data.forEach(city => {
                                $('select[name=city_id]').append(`<option value="${city.id}">${city.name}</option>`);

                            });
                            console.log(data)
                        })
                        .catch(error => {
                            console.log(error)
                        })
            });
        });
    </script>


@endpush
