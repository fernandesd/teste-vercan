@extends('layouts.app')

@section('title', 'Edição de Fornecedor')

@section('content_header')
    <div class="row mb-3">
        <div class="col-md-10">
            <h1>Fornecedor - <small>Editar</small> </h1>
        </div>
    </div>
@stop

@section('content')
{{ Form::model($supplier, ['route'=> ['admin.suppliers.update', $supplier->id], 'method'=>'PUT', 'id' => 'formUpdate', 'class'=>'pb-4']) }}

    @include('admin.suppliers._form')

    <x-adminlte-button class="btn-flat" type="submit" label="Salvar" theme="success" icon="fas fa-lg fa-save"/>
{{ Form::close() }}
@stop

@push('js')
{!! JsValidator::formRequest('App\Http\Requests\Admin\SupplierRequest', '#formUpdate'); !!}
<script>
    $(document).ready(function(){
        $('#formUpdate').on('submit', ()=> $(".form-check-inline #type-error").hide());
    });
</script>

@endpush
