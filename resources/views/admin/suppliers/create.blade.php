@extends('layouts.app')

@section('title', 'Fornecedor Criar')

@section('content_header')
    <div class="row mb-3">
        <div class="col-md-10">
            <h1>Fornecedor - <small>Criar</small> </h1>
        </div>
    </div>
@stop

@section('content')
{{ Form::open(['route'=> ['admin.suppliers.store'], 'method'=>'POST', 'id' => 'formCreate', 'class'=>'pb-4']) }}

    @include('admin.suppliers._form')

    <x-adminlte-button class="btn-flat" type="submit" label="Salvar" theme="success" icon="fas fa-lg fa-save"/>
{{ Form::close() }}
@stop

@push('js')
{!! JsValidator::formRequest('App\Http\Requests\Admin\SupplierRequest', '#formCreate'); !!}
<script>
    $(document).ready(function(){
        $('#formCreate').on('submit', ()=> $(".form-check-inline #type-error").hide());
    });
</script>

@endpush
