@extends('layouts.app')

@section('title', 'Fornecedor Excluir')

@section('content_header')
    <div class="row mb-3">
        <div class="col-md-10">
            <h1>Fornecedor - <small>Excluir</small> </h1>
        </div>
    </div>
@stop

@section('content')
<x-adminlte-card title="Registro" theme="light" icon="fas fa-lg fa-exclamation" collapsible>
    <div class="form-group">
        @if($supplier->cpf)
        <label class="control-label"> Nome/Apelido </label>
        <input type="text" class="form-control" value="{{$supplier->name}}/{{$supplier->nickname}}" readonly="">
        @else
        <label class="control-label"> Razão Social/Nome Fantasia </label>
        <input type="text" class="form-control" value="{{$supplier->cnpj_legal_name}}/{{$supplier->cnpj_trade_name}}" readonly="">
        @endif
    </div>
</x-adminlte-card>
{{ Form::open(['route'=> ['admin.suppliers.destroy', $supplier->id], 'method'=>'DELETE', 'id' => 'formDelete']) }}
<button type="submit" onClick="" class="btn btn-sm btn-danger">
    <i class="fas fa-sm fa-trash"></i> Excluir
</button>
{{ Form::close() }}

@stop

@section('js')
<script>
$('form#formDelete').submit((e)=>{
    e.preventDefault();
    const confirmed = confirm('Tem certeza que deseja excluir esse fornecedor?');
    if(confirmed) { e.target.submit(); }
})
</script>
@endsection
