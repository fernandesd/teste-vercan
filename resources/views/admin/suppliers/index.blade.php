@extends('layouts.app')

@section('title', 'Fornecedores')

@section('content_header')
    <div class="row mb-3">
        <div class="col-md-10">
            <h1>Fornecedores - <small>Painel de Controle</small> </h1>
        </div>
        <div class="col-md-2">
            <a href="{{route('admin.suppliers.create')}}">
                <button class="btn btn-block btn-success" z="">
                    <i class="fa fa-plus fa-fw"></i> Cadastrar
                </button>
            </a>
        </div>
    </div>
@stop

@section('content')
<section class="p-4 bg-white">
    {{-- Table --}}
    <div class="table-responsive">
        <table style="width:100%" class="table table-bordered datatable">
            {{-- Table head --}}
            <thead>
                <tr>
                    <th>Razão Social/Nome</th>
                    <th>Nome Fantasia/Apelido</th>
                    <th>CNPJ/CPF</th>
                    <th>Ativo</th>
                    <th>Ação</th>
                </tr>
            </thead>
                @foreach ($suppliers as $supplier)
                <tr>
                    <td>{{ $supplier->cnpj_legal_name ?? $supplier->name }}</td>
                    <td>{{ $supplier->cnpj_trade_name ?? $supplier->nickname }}</td>
                    <td class="@if($supplier->cnpj) cnpj @else cpf @endif">{{ $supplier->cnpj ?? $supplier->cpf }}</td>
                    <td>{{ $supplier->is_active_str }}</td>
                    <td>
                        <a href="{{route('admin.suppliers.edit', [$supplier->id])}}" class="btn btn-xs btn-default text-primary mx-1 shadow" title="Editar">
                            <i class="fa fa-lg fa-fw fa-pen"></i>
                        </a>
                        <a href="{{route('admin.suppliers.show', [$supplier->id])}}" class="btn btn-xs btn-default text-teal mx-1 shadow" title="Ver">
                            <i class="fa fa-lg fa-fw fa-eye"></i>
                        </a>
                        <a href="{{route('admin.suppliers.delete', [$supplier->id])}}" class="btn btn-xs btn-default text-danger mx-1 shadow" title="Excluir">
                            <i class="fa fa-lg fa-fw fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            <tbody>
            </tbody>
        </table>
    </div>
</section>
@stop

@push('css')
<style type="text/css">
    .datatable tr td,  .datatable tr th {
        vertical-align: middle;
    }
</style>
@endpush

@push('js')
<script>
    $(() => {
        $('.datatable').DataTable({
            lengthMenu: [10, 50, 100, 200],
            order: [
                [0, "asc"]
            ],
            language: {
                url: "{{asset('/vendor/datatables/js/pt-br.json')}}"
            },
        });
    })
</script>
<script>
$(document).ready(function(){
    $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
});
</script>
@endpush
