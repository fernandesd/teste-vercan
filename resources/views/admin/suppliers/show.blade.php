@extends('layouts.app')

@section('title', 'Fornecedor Editar')

@section('content_header')
    <div class="row mb-3">
        <div class="col-md-10">
            <h1>Fornecedor - <small>Ver</small> </h1>
        </div>
    </div>
@stop

@section('content')
{{ Form::model($supplier, ['route'=> ['admin.suppliers.store'], 'method'=>'PUT', 'id' => 'formCreate', 'class'=>'pb-4']) }}

<fieldset disabled>
    @include('admin.suppliers._form')
</fieldset>

<a href="{{route('admin.suppliers.edit', [$supplier->id])}}" class="btn btn-sm btn-primary">
    <i class="fa fa-edit fa-fw"></i> Editar
</a>
{{ Form::close() }}
@stop

@push('js')
{!! JsValidator::formRequest('App\Http\Requests\Admin\Supplier\StoreRequest', '#formCreate'); !!}
<script>
    $(document).ready(function(){
        $('.summernote').summernote('disable');
    });
</script>

@endpush
