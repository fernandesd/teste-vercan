#!/bin/bash

echo "[Copy docker-compose.development.yml to docker-compose.yml]"
cp docker-compose.development.yml docker-compose.yml

echo "[Start containers]"
docker compose up -d

echo "[Copy .env.example to .env]"
cp .env.example .env

echo "[Install dependencies]"
docker compose exec laravel composer install
docker compose exec laravel npm install

echo "[Generate key]"
docker compose exec laravel php artisan key:generate



echo "App is running on: http://localhost/"
