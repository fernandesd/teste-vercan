<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::namespace('App\Http\Controllers\Admin')->middleware(['auth'])->name('admin.')->prefix('admin')->group(function () {
    Route::get('/', 'HomeController@index')->name('index');

    Route::resource('fornecedores', 'SupplierController')->names('suppliers')->parameters([
        'fornecedores' => 'fornecedor',
    ]);
    Route::get('fornecedores/{fornecedor}/deletar', 'SupplierController@delete')->name('suppliers.delete');
});
