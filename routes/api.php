<?php

use App\Http\Controllers\Api\HelperController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\FlareClient\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/ping', fn () => response()->json('pong'))->name('api.ping');

Route::controller(HelperController::class)->name('api.')->group(function () {
    Route::get('/dados-cep/{cep}', 'getCepData')->name('cep');
    Route::get('/dados-cnpj/{cnpj}', 'getCnpjData')->name('cnpj');
    Route::get('/estado/{stateId}/cidades', 'getCitiesDataFromState')->name('state');
});
