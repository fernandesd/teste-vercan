# Gestão de Fornecedores By Vercan
Este é um teste aplicado para testar os conhecimentos de Laravel.
### Principais tecnologias utilizadas
 - PHP 8.2
 - Composer 2
 - Laravel 10
 - Mysql 8.0
 - Docker 20.10.22
 - Git
### Como instalar (necessário ter docker instalado):
 1. Clonar o projeto
 2. `cd teste-vercan`
 3. `./.scripts/dev-install.sh`
 4. `docker compose exec laravel php artisan migrate --seed` 
 6. A aplicação estará disponível em: [http://localhost](http://localhost)
 - Executando comandos: `docker compose exec laravel php artisan tinker`
### Rodando testes
 - Migrar o banco de dados de teste: `docker compose exec laravel php artisan migrate --seed --env=testing`
 - Executar os testes: `docker compose exec laravel php artisan test`

 ### Rotas de api

- /api/dados-cep/{cep} - Busca Endereço
- /api/dados-cnpj/{cnpj} - Busca Empresa
- /api/estado/{uf_id}/cidades - Lista cidades da UF

### Usuário admin
- login: admin@admin.com
- senha: password
