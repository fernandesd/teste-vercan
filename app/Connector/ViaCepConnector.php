<?php

namespace App\Connector;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class ViaCepConnector
{
    public $baseUrl;

    public function __construct()
    {
        $this->baseUrl = config('connector.viaCep.baseUrl');
    }

    /**
     * Envia uma requisição GET síncrona
     *
     * @param  string  $endpoint - endpoint que será concatenado a baseUrl
     * @return Response - retorno da requisição
     */
    public function getSync(string $endpoint): Response
    {
        return Http::get("{$this->baseUrl}/{$endpoint}");
    }
}
