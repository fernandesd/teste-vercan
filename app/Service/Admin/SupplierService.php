<?php

namespace App\Service\Admin;

use App\Models\Contact;
use App\Models\Supplier;

class SupplierService
{
    /**
     * Salva o Fornecedor e suas relações de contatos e endereço
     *
     * @param  Supplier  $supplier - Instancia da model
     * @param  array<string, mixed>  $requestData - dados da request após validação
     */
    public function createSupplier($requestData): void
    {
        $supplier = Supplier::create($requestData);
        $this->saveSupplierContacts($supplier, $requestData);
        $this->saveSupplierAddress($supplier, $requestData);
    }

    /**
     * Atualiza o Fornecedor e suas relações de contatos e endereço
     *
     * @param  Supplier  $supplier - Instancia da model
     * @param  array<string, mixed>  $requestData - dados da request após validação
     */
    public function updateSupplier(Supplier $supplier, array $requestData): void
    {
        $supplier->update($requestData);
        $this->saveSupplierContacts($supplier, $requestData);
        $this->saveSupplierAddress($supplier, $requestData);
    }

    /**
     * Salva o endereço no banco de dados
     *
     * @param  Supplier  $supplier - Instancia da model
     * @param  array<string, mixed>  $requestData - dados da request após validação
     */
    public function saveSupplierAddress(Supplier $supplier, array $requestData): void
    {
        $supplier->address()->updateOrCreate(['zipcode' => $requestData['zipcode']], $requestData);
    }

    /**
     * Salva os contatos no banco de dados
     *
     * @param  Supplier  $supplier - Instancia da model
     * @param  array<string, mixed>  $requestData - dados da request após validação
     */
    public function saveSupplierContacts(Supplier $supplier, array $requestData): void
    {
        $contactsData = $this->concatContactsDataToSave($requestData);
        $supplier->contacts()->forceDelete();
        $supplier->contacts()->createMany($contactsData);
    }

    /**
     * Unifica os contatos principais e os contatos adicionais em um array
     *
     * @param  array<string, mixed>  $requestData - dados da request após validação
     * @return array<string, mixed> - contatos principais e contatos adicionais concatenados prontos para salvar no banco de dados
     */
    public function concatContactsDataToSave(array $requestData): array
    {
        return [...$this->getMainContactData($requestData['contact']), ...$this->handleAdditionalContactData($requestData['contact'])];
    }

    /**
     * Monta o array com os dados dos contatos adicionais
     *
     * @param  array<string, mixed>  $contactDataRequest - dados do contato recebido na request
     * @return array<string, mixed> - dados dos contatos adicionais
     */
    public function handleAdditionalContactData(array $contactDataRequest): array
    {
        $nonRepeatAttributtes = collect($contactDataRequest['additional'])->only(['name', 'company', 'role'])->toArray();
        $nonRepeatAttributtes['type'] = Contact::CONTACT_ADDITIONAL;

        return collect($contactDataRequest['additional']['informations'])
            ->map(fn ($item) => [...$nonRepeatAttributtes, ...$item])->toArray();
    }

    /**
     * Extrai os dados dos contatos principais da request
     *
     * @param  array<string, mixed>  $contactDataRequest - dados do contato recebido na request
     * @return array<string, mixed> - dados dos contatos principais
     */
    public function getMainContactData(array $contactDataRequest): array
    {
        return $contactDataRequest['main'];
    }
}
