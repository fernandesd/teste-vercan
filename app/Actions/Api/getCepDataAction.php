<?php

namespace App\Actions\Api;

use App\Connector\ViaCepConnector;

class getCepDataAction
{
    public function run(string $cep)
    {
        return (new ViaCepConnector())->getSync("{$cep}/json");
    }
}
