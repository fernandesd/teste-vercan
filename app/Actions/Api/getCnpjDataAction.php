<?php

namespace App\Actions\Api;

use App\Connector\ReceitaAwsConnector;

class getCnpjDataAction
{
    public function run(string $cnpj)
    {
        return (new ReceitaAwsConnector())->getSync("{$cnpj}");
    }
}
