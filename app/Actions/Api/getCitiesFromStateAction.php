<?php

namespace App\Actions\Api;

use App\Models\State;

class getCitiesFromStateAction
{
    public function run(State $state)
    {
        return $state->cities;
    }
}
