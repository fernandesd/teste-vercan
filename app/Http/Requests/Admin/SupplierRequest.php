<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $data = [
            'cnpj' => 'nullable|cnpj|string|required_if:type,0',
            'cnpj_legal_name' => 'nullable|string|required_if:type,0',
            'cnpj_trade_name' => 'nullable|string|required_if:type,0',
            'cnpj_ie_indicator' => 'nullable|numeric|required_if:type,0',
            'cnpj_ie' => 'nullable|string|required_if:cnpj_ie_indicator,0',
            'cnpj_im' => 'nullable|string|nullable',
            'cnpj_status' => 'string|nullable',
            'cnpj_tax_collection_type' => 'nullable|numeric|required_if:type,0',
            'is_active' => 'numeric|required',
            'cpf' => 'nullable|string|cpf|required_if:type,1',
            'name' => 'nullable|string|required_if:type,1',
            'nickname' => 'string|nullable',
            'rg' => 'nullable|string|required_if:type,1',
            'description' => 'string|nullable',
            'contact.main.*.phone' => 'string|required',
            'contact.main.*.phone_type' => 'numeric|required',
            'contact.main.*.email' => 'email|nullable',
            'contact.main.*.email_type' => 'numeric|nullable',
            'contact.*.name' => 'string|nullable',
            'contact.*.company' => 'string|nullable',
            'contact.*.role' => 'string|nullable',
            'contact.additional.informations.*.phone' => 'string|nullable',
            'contact.additional.informations.*.phone_type' => 'numeric|nullable',
            'contact.additional.informations.*.email' => 'email|nullable',
            'contact.additional.informations.*.email_type' => 'numeric|nullable',
            'zipcode' => 'string|required',
            'street' => 'string|required',
            'number' => 'string|required',
            'complement' => 'string|nullable',
            'district' => 'string|required',
            'reference' => 'string|nullable',
            'state_id' => 'numeric|required',
            'city_id' => 'numeric|required',
            'is_condominium' => 'numeric|required',
            'end_cond' => 'nullable|string|required_if:is_condominium,1',
            'number_cond' => 'nullable|string|required_if:is_condominium,1',
        ];

        return $this->routeIs('admin.suppliers.store') ? [...$data, ...['type' => 'required']] : $data;
    }
}
