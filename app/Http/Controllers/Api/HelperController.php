<?php

namespace App\Http\Controllers\Api;

use App\Actions\Api\getCepDataAction;
use App\Actions\Api\getCitiesFromStateAction;
use App\Actions\Api\getCnpjDataAction;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\JsonResponse;

class HelperController extends Controller
{
    /**
     * Obtém os dados de endereço a partir do CEP recebido
     *
     * @param  string  $cep - CEP a ser buscado
     *
     * @var City - Instância da cidade pertencente ao cep buscado
     *
     * @return JsonResponse - dados do endereço encontrado
     */
    public function getCepData(string $cep): JsonResponse
    {
        $cepResponseData = (new getCepDataAction())->run($cep)->json();
        $cityIdAttributes = City::Where('name', $cepResponseData['localidade'])->get(['id', 'state_id'])->first();
        $stateCityDbIds = ['stateDbId' => $cityIdAttributes->state_id, 'cityDbId' => $cityIdAttributes->id];

        return response()->json([...$cepResponseData, ...$stateCityDbIds]);
    }

    /**
     * Obtém os dados da empresa a partir do CNPJ recebido
     *
     * @param  string  $cnpj - CNPJ a ser buscado
     * @return JsonResponse - dados da empresa encontrada
     */
    public function getCnpjData(string $cnpj): JsonResponse
    {
        return response()->json((new getCnpjDataAction())->run($cnpj)->json());
    }

    /**
     * Lista as cidades de um estado
     *
     * @param  State  $state - Instância do estado a ser buscado
     * @return JsonResponse - lista das cidades relacionadas ao estado
     */
    public function getCitiesDataFromState(State $state): JsonResponse
    {
        return response()->json((new getCitiesFromStateAction())->run($state));
    }
}
