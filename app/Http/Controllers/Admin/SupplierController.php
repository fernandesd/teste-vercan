<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SupplierRequest;
use App\Models\Address;
use App\Models\Contact;
use App\Models\State;
use App\Models\Supplier;
use App\Service\Admin\SupplierService;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::select(['id', 'cnpj_legal_name', 'name', 'nickname', 'cnpj_trade_name', 'cnpj', 'cpf', 'is_active'])->get();

        return view('admin.suppliers.index', compact('suppliers'));
    }

    public function create($edit = false)
    {
        return view('admin.suppliers.create', compact('edit'));
    }

    public function store(SupplierRequest $request)
    {
        (new SupplierService())->createSupplier($request->validated());
        toast('Fornecedor criado com sucesso.', 'success');

        return redirect()->route('admin.suppliers.index');
    }

    /**
     * @param  Supplier  $supplier - Instância da model.
     * @param  bool  $edit - variável de controle utilizada no form.
     *
     * @var Address address - Instacia da relação de endereço do fornecedor
     * @var Contact contacts - Collection de contatos do fornecedor
     * @var Illuminate\Database\Eloquent\Builder - Contatos principais
     * @var Illuminate\Database\Eloquent\Builder - Contatos adicionais
     * @var Contact - Dados do primeiro contato adicional da lista
     * @var Illuminate\Support\Collection - Lista das cidades para popular o form
     */
    public function show(Supplier $supplier, $edit = true)
    {
        $address = $supplier->address;
        $contacts = $supplier->contacts;
        $mainContacts = $contacts->where('type', Contact::CONTACT_MAIN);
        $additionalContacts = $contacts->where('type', Contact::CONTACT_ADDITIONAL);
        $additionalContact = $additionalContacts->first();
        $cities = State::find($address->state_id)->cities()->orderBy('name')->pluck('name', 'id');

        return view('admin.suppliers.show', compact('supplier', 'edit', 'address', 'mainContacts', 'additionalContacts', 'additionalContact', 'cities'));
    }

    /**
     * @param  Supplier  $supplier - Instância da model.
     * @param  bool  $edit - variável de controle utilizada no form.
     *
     * @var Address address - Instacia da relação de endereço do fornecedor
     * @var Contact contacts - Collection de contatos do fornecedor
     * @var Illuminate\Database\Eloquent\Builder - Contatos principais
     * @var Illuminate\Database\Eloquent\Builder - Contatos adicionais
     * @var Contact - Dados do primeiro contato adicional da lista
     * @var Illuminate\Support\Collection - Lista das cidades para popular o form
     */
    public function edit(Supplier $supplier, $edit = true)
    {
        $address = $supplier->address;
        $contacts = $supplier->contacts;
        $mainContacts = $contacts->where('type', Contact::CONTACT_MAIN);
        $additionalContacts = $contacts->where('type', Contact::CONTACT_ADDITIONAL);
        $additionalContact = $additionalContacts->first();
        $cities = State::find($address->state_id)->cities()->orderBy('name')->pluck('name', 'id');

        return view('admin.suppliers.edit', compact('supplier', 'edit', 'address', 'mainContacts', 'additionalContacts', 'additionalContact', 'cities'));
    }

    public function update(SupplierRequest $request, Supplier $supplier)
    {
        (new SupplierService())->updateSupplier($supplier, $request->validated());
        toast('Fornecedor atualizado com sucesso.', 'success');

        return redirect()->route('admin.suppliers.index');
    }

    public function destroy(Supplier $supplier)
    {
        $supplier->delete();

        toast('Fornecedor removido com sucesso.', 'success');

        return redirect()->route('admin.suppliers.index');
    }

    /**
     * Exibe a view para deletar Fornecedor
     */
    public function delete(Supplier $supplier)
    {
        return view('admin.suppliers.delete', compact('supplier'));
    }
}
