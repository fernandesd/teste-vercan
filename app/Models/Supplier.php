<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'cnpj',
        'cnpj_legal_name',
        'cnpj_trade_name',
        'cnpj_ie_indicator',
        'cnpj_ie',
        'cnpj_im',
        'cnpj_status',
        'cnpj_tax_collection_type',
        'is_active',
        'cpf',
        'name',
        'nickname',
        'rg',
        'description',
        'type',
    ];

    const IE_INDICATOR = [
        0 => 'Contribuinte',
        1 => 'Contribuinte Isento',
        2 => 'Não Contribuinte',
    ];

    const TAX_COLLECTON_TYPE = [
        0 => 'A Recolher Pelo Prestador',
        1 => 'Retido Pelo Tomador',
    ];

    const IS_ACTIVE = [
        0 => 'Não',
        1 => 'Sim',
    ];

    const TYPES = [
        0 => 'Pessoa Jurídica',
        1 => 'Pessoa Física',
    ];

    public function getIsActiveStrAttribute()
    {
        return $this::IS_ACTIVE[$this->is_active];
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class)->withDefault();
    }
}
