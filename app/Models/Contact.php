<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'phone',
        'phone_type',
        'email',
        'email_type',
        'type',
        'name',
        'company',
        'role',
    ];

    const PHONE_TYPES = [
        0 => 'Residencial',
        1 => 'Comercial',
        2 => 'Celular',
    ];

    const EMAIL_TYPES = [
        0 => 'Pessoal',
        1 => 'Comercial',
        2 => 'Outro',
    ];

    const CONTACT_MAIN = 0;

    const CONTACT_ADDITIONAL = 1;

    const CONTACT_TYPES = [
        self::CONTACT_MAIN => 'Principal',
        self::CONTACT_ADDITIONAL => 'Adicional',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}
