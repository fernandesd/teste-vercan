<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'zipcode',
        'street',
        'number',
        'complement',
        'district',
        'reference',
        'state_id',
        'city_id',
        'is_condominium',
        'end_cond',
        'number_cond',
    ];

    const IS_CONDOMINIUM = [
        0 => 'Não',
        1 => 'Sim',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
