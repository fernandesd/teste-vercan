<?php

namespace Tests\Feature;

use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function testCheckApi(): void
    {
        $response = $this->getJson(route('api.ping'));

        $response->assertSuccessful();
        $response->assertContent('"pong"');
    }
}
