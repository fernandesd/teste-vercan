<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class HomeTest extends TestCase
{
    /**
     * Verifica se o usuário é redirecionado ao acessar a home
     */
    public function testCheckIfRedirectToLoginOnHomePage(): void
    {
        $response = $this->get('/');

        $response->assertStatus(302);
        $response->assertRedirectToRoute('login', $parameters = []);

    }

    /**
     * Verifica se o usuário deslogado não consegue acessar a dashboard
     */
    public function testCheckIfVisitorCantAccessDashboard(): void
    {
        $response = $this->get(route('admin.index'));

        $response->assertStatus(302);
        $response->assertRedirectToRoute('login', $parameters = []);
    }

    /**
     * Verifica se o usuário logado consegue acessar a dashboard
     */
    public function testUserCanAccessDashboard(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->get(route('admin.index'));

        $response->assertStatus(200);
    }
}
