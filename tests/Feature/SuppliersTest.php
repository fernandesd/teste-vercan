<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Contact;
use App\Models\State;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SuppliersTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create();
        $this->actingAs($user);
    }

    /**
     * Verifica a página de listagem de Fornecedores
     */
    public function testListSuppliers()
    {
        $this->get(route('admin.suppliers.index'))
            ->assertSuccessful()
            ->assertSee('Fornecedores')
            ->assertSee('Painel de Controle')
            ->assertSee('Razão Social/Nome')
            ->assertSee('Nome Fantasia/Apelido')
            ->assertSee('CNPJ/CPF')
            ->assertSee('Ativo')
            ->assertSee('Ação');
    }

    /**
     * Verifica criação de Fornecedores
     */
    public function testCreateSupplier()
    {
        State::factory(10)->hasCities(10)->create();

        $supplierData = $this->getSupplierData();

        $this->get(route('admin.suppliers.create'))
            ->assertStatus(200)
            ->assertSee('Dados do Fornecedor')
            ->assertSee('Contato Principal')
            ->assertSee('Contatos Adicionais')
            ->assertSee('Dados de Endereço')
            ->assertSee('Observação')
            ->assertSee('Pessoa Jurídica')
            ->assertSee('Pessoa Física');

        $this->post(route('admin.suppliers.store'), $supplierData)
            ->assertStatus(302)
            ->assertRedirectToRoute('admin.suppliers.index');
    }

    /**
     * Verifica a página de edição e a atualização do Fornecedor
     */
    public function testUpdateSuppliers()
    {
        State::factory(10)->hasCities(10)->create();

        $supplier = Supplier::factory()
            ->hasContacts(3, fn () => [
                'type' => 1,
            ])->hasAddress(1)
            ->create([
                'cnpj' => null,
                'cnpj_legal_name' => null,
                'cnpj_trade_name' => null,
                'cnpj_ie_indicator' => null,
                'cnpj_ie' => null,
                'cnpj_im' => null,
                'cnpj_status' => null,
                'cnpj_tax_collection_type' => null,
                'type' => 1,
            ]);

        $supplierDataToUpdate = $this->getSupplierData();

        $this->get(route('admin.suppliers.edit', [$supplier->id]))
            ->assertStatus(200)
            ->assertSee('Edição de Fornecedor')
            ->assertSee('Nome')
            ->assertSee('CNPJ')
            ->assertSee('Endereço')
            ->assertSee('Telefone')
            ->assertSee('Salvar');

        $this->put(route('admin.suppliers.update', [$supplier->id]), $supplierDataToUpdate)
            ->assertStatus(302)
            ->assertRedirectToRoute('admin.suppliers.index');
    }

    /**
     * Verifica a exclusão de um fornecedor
     */
    public function testDeleteSupplier()
    {
        $supplier = Supplier::factory()->create();
        $this->delete(route('admin.suppliers.destroy', [$supplier->id]))
            ->assertStatus(302)
            ->assertRedirectToRoute('admin.suppliers.index');

        $this->assertNull(Supplier::find($supplier->id));
        $this->assertSoftDeleted($supplier);
    }

    public function getSupplierData()
    {
        $contactData = Contact::factory(3)->make([
            'name' => null,
            'company' => null,
            'role' => null,
            'type' => 0,
        ])->toArray();

        $addressData = Address::factory()->make()->toArray();

        $supplierData = Supplier::factory()->make([
            'cpf' => null,
            'name' => null,
            'nickname' => null,
            'rg' => null,
            'type' => Contact::CONTACT_MAIN,
        ])->toArray();

        $additionalContact = [
            'name' => null,
            'company' => null,
            'role' => null,
            'informations' => [
                [
                    'phone' => null,
                    'phone_type' => null,
                    'email' => null,
                    'email_type' => null,
                ],
            ],
        ];

        $supplierDataToUpdate = [
            ...$supplierData,
            ...['contact' => [
                'main' => $contactData,
                'additional' => $additionalContact]],
            ...$addressData];

        return $supplierDataToUpdate;
    }
}
