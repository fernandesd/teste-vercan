<?php

namespace Database\Factories;

use App\Models\State;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $stateId = $this->faker->randomElement(State::pluck('id'));
        $citiesEloquentCollection = State::find($stateId)->cities();

        return [
            'zipcode' => $this->faker->postcode(),
            'street' => $this->faker->streetName(),
            'number' => $this->faker->buildingNumber(),
            'complement' => $this->faker->sentence(),
            'district' => $this->faker->state(),
            'reference' => $this->faker->sentence(),
            'state_id' => (string) $stateId,
            'city_id' => (string) $this->faker->randomElement($citiesEloquentCollection->pluck('id')),
            'is_condominium' => $this->faker->randomElement([0]),
        ];
    }
}
