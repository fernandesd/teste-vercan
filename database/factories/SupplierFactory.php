<?php

namespace Database\Factories;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Supplier>
 */
class SupplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'cnpj' => $this->faker->cnpj(false),
            'cnpj_legal_name' => $this->faker->company(),
            'cnpj_trade_name' => $this->faker->companySuffix(),
            'cnpj_ie_indicator' => $this->faker->randomKey(Supplier::IE_INDICATOR),
            'cnpj_ie' => '752359371',
            'cnpj_im' => null,
            'cnpj_status' => $this->faker->randomElement(['ATIVA', 'BAIXADA']),
            'cnpj_tax_collection_type' => $this->faker->randomKey(Supplier::TAX_COLLECTON_TYPE),
            'is_active' => $this->faker->randomKey(Supplier::IS_ACTIVE),
            'cpf' => $this->faker->cpf(false),
            'name' => $this->faker->name(),
            'nickname' => $this->faker->lastName(),
            'rg' => $this->faker->rg(false),
            'description' => $this->faker->realText(),
            'type' => $this->faker->randomKey(Supplier::TYPES),
        ];
    }
}
