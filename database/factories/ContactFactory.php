<?php

namespace Database\Factories;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contact>
 */
class ContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'phone' => $this->faker->phoneNumber(),
            'phone_type' => $this->faker->randomKey(Contact::PHONE_TYPES),
            'email' => $this->faker->email(),
            'email_type' => $this->faker->randomKey(Contact::EMAIL_TYPES),
            'type' => $this->faker->randomKey(Contact::CONTACT_TYPES),
            'name' => $this->faker->name(),
            'company' => $this->faker->company(),
            'role' => $this->faker->jobTitle(),
        ];
    }
}
