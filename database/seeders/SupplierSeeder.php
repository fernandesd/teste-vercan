<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /** Fornecedor pessoa jurídica */
        Supplier::factory(10)
            ->hasContacts(3, fn () => [
                'name' => null,
                'company' => null,
                'role' => null,
                'type' => 0,
            ])->hasAddress(1)
            ->create([
                'cpf' => null,
                'name' => null,
                'nickname' => null,
                'rg' => null,
                'type' => 0,
            ]);

        /** Fornecedor pessoa física */
        Supplier::factory(10)
            ->hasContacts(3, fn () => [
                'type' => 1,
            ])->hasAddress(1)
            ->create([
                'cnpj' => null,
                'cnpj_legal_name' => null,
                'cnpj_trade_name' => null,
                'cnpj_ie_indicator' => null,
                'cnpj_ie' => null,
                'cnpj_im' => null,
                'cnpj_status' => null,
                'cnpj_tax_collection_type' => null,
                'type' => 1,
            ]);
    }
}
