<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('cnpj')->nullable();
            $table->string('cnpj_legal_name')->nullable();
            $table->string('cnpj_trade_name')->nullable();
            $table->integer('cnpj_ie_indicator')->nullable();
            $table->string('cnpj_ie')->nullable();
            $table->string('cnpj_im')->nullable();
            $table->string('cnpj_status')->nullable();
            $table->integer('cnpj_tax_collection_type')->nullable();
            $table->boolean('is_active');
            $table->string('cpf')->nullable();
            $table->string('name')->nullable();
            $table->string('nickname')->nullable();
            $table->string('rg')->nullable();
            $table->mediumText('description')->nullable();
            $table->integer('type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('suppliers');
    }
};
